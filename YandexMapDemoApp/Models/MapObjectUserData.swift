//
//  MapObjectUserData.swift
//  YandexMapDemoApp
//
//  Created by Anton on 23.09.2021.
//

import Foundation

struct MapObjectUserData {
    let id = UUID()
    let description: String
}
