//
//  PlacemarkInfoAlert.swift
//  YandexMapDemoApp
//
//  Created by Anton on 23.09.2021.
//

import SwiftUI

struct PlacemarkInfoAlert: View {
    @Binding var showAlert: Bool
    let callback: () -> Void
    
    var body: some View {
        VStack(spacing: 8) {
            Text("Прокуратура какой-то обрасти")
                .font(.system(size: 9))
            Text("здесь адрес прокуратуры")
                .font(.system(size: 7))
            Divider()
                .padding(.horizontal, 50)
            Text("Построить маршрут в Яндекс картах?")
                .font(.system(size: 9))
            HStack {
            Button("OK") {
                callback()
                print("строится маршрут")
            }
                Button("Отмена") {
                    showAlert.toggle()
                }
            }
        }
        .padding()
        .foregroundColor(.white)
        .background(Color.blue)
        .cornerRadius(15)
    }
}

struct PlacemarkInfoAlert_Previews: PreviewProvider {
    static var previews: some View {
        PlacemarkInfoAlert(showAlert: .constant(false), callback: {})
            
    }
}
