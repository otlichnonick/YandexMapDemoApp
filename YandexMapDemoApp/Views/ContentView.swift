//
//  ContentView.swift
//  YandexMapDemoApp
//
//  Created by Anton on 22.09.2021.
//

import SwiftUI
import CoreLocation

struct ContentView: View {
    @StateObject var manager = LocationManager()
    @State private var showSheet = false
    @State private var showAlert = false
    
    var body: some View {
        ZStack {
            YMKViewContainer(manager: manager, callback: { objectData in
                showAlert.toggle()
            })
                .actionSheet(isPresented: $showSheet) {
                    ActionSheet(title: Text("Выберите прокуратуру"), buttons: [
//                        .default(Text("Текущее положение")) {
//                            manager.location = manager.manager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 50, longitude: 50)
//                        },
                        .default(Text("в Москве")) {
                            manager.location = Constants.procInMoscow
                        },
                        .default(Text("в Иванове")) {
                            manager.location = Constants.procInIvanovo
                        },
                        .default(Text("в Петербурге")) {
                            manager.location = Constants.procInPeterburg
                        },
                        .cancel()
                    ])
                }
            VStack {
                Spacer()
                
                Button("Выбрать прокуратуру") {
                    self.showSheet.toggle()
                }
                .padding()
                .background(Color.blue)
                .foregroundColor(.black)
                .frame(height: 40, alignment: .center)
                .cornerRadius(10)
                .padding(.bottom, 20)
            }
            
            if showAlert {
                PlacemarkInfoAlert(showAlert: $showAlert, callback: { })
                    .frame(width: UIScreen.main.bounds.size.width * 0.5)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
