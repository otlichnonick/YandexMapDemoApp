//
//  YMKViewContainer.swift
//  YandexMapDemoApp
//
//  Created by Anton on 22.09.2021.
//

import UIKit
import SwiftUI
import YandexMapsMobile
import CoreLocation

struct YMKViewContainer: UIViewRepresentable {
    @ObservedObject var manager: LocationManager
    @State private var mapView = YMKMapView()
    let callback: (MapObjectUserData) -> Void
    @State private var mapObjectTapListener: TapListener!
    @State private var drivingSession: YMKDrivingSession?
    var drivingListener = DrivingListener()
    
    func makeUIView(context: Context) -> YMKMapView {
        mapView.mapWindow.map.isRotateGesturesEnabled = false
        drivingListener.mapView = mapView
        drivingListener.startLocation = manager.currentLocation
        drivingListener.endLocation = manager.location
        drivingListener.drivingSession = drivingSession
        
        return mapView
    }
    
    func updateUIView(_ uiView: YMKMapView, context: Context) {
        showPointOn(uiView, scale: 15)
        drivingListener.startLocation = manager.currentLocation
        drivingListener.endLocation = manager.location
        drivingListener.followTheRouteWith(uiView)
    }
    
    private func showPointOn(_ uiView: YMKMapView, scale: Float) {
        if let location = manager.location {
            uiView.mapWindow.map.move(
                with: YMKCameraPosition(target: YMKPoint(latitude: location.latitude, longitude: location.longitude), zoom: scale, azimuth: 0, tilt: 0)/*,
                animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 3),
                cameraCallback: nil*/)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                createPlacemarkIn(location)
            }
        }
    }
    
    private func createPlacemarkIn(_ point: CLLocationCoordinate2D) {
        let mapObjects = mapView.mapWindow.map.mapObjects
        let image = UIImage(contentsOfFile: Bundle.main.path(forResource: "placemark-md", ofType: "png")!)!
        let placemark = mapObjects.addPlacemark(with: YMKPoint(latitude: point.latitude, longitude: point.longitude), image: image)
        placemark.zIndex = 100
        placemark.userData = MapObjectUserData(description: "test description")
        mapObjectTapListener = TapListener(callback: callback)
        placemark.addTapListener(with: mapObjectTapListener)
    }
}

protocol YMKMapDrivingListener {
    var startLocation: CLLocationCoordinate2D? { get set }
    var endLocation: CLLocationCoordinate2D? { get set }
    var mapView: YMKMapView? { get set }
    var drivingSession: YMKDrivingSession? { get set }
    
    func followTheRouteWith(_ mapView: YMKMapView)
}

class DrivingListener: NSObject {
    var startLocation: CLLocationCoordinate2D?
    var endLocation: CLLocationCoordinate2D?
    var mapView: YMKMapView?
    var drivingSession: YMKDrivingSession?
}

extension DrivingListener: YMKMapDrivingListener {
    func followTheRouteWith(_ mapView: YMKMapView) {
        if canShowRoute(from: startLocation, to: endLocation) {
        print("****************hello****************")
            getRoute(from: YMKPoint(latitude: startLocation!.latitude, longitude: startLocation!.longitude),
                     to: YMKPoint(latitude: endLocation!.latitude, longitude: endLocation!.longitude))
//            getRoute(from: YMKPoint(latitude: Constants.procInPeterburg.latitude, longitude: Constants.procInPeterburg.longitude), to: YMKPoint(latitude: Constants.procInMoscow.latitude, longitude: Constants.procInMoscow.longitude))
   
        } else {
            print("****************can not build the route****************")
        }
    }
    
    private func canShowRoute(from start: CLLocationCoordinate2D?, to end: CLLocationCoordinate2D?) -> Bool {
            return start != nil && end != nil ? true : false
    }
    
    private func getRoute(from start: YMKPoint, to end: YMKPoint) {
        let requestPoints: [YMKRequestPoint] = [
            YMKRequestPoint(point: start, type: .waypoint, pointContext: nil),
            YMKRequestPoint(point: end, type: .waypoint, pointContext: nil)
        ]
        print("--------------start: \(start)")
        print("--------------end: \(end)")
        print("--------------requestPoints: \(requestPoints)")
        
        let responseHandler = {(routesResponse: [YMKDrivingRoute]?, error: Error?) -> Void in
            if let routes = routesResponse {
                print("--------------routes: \(routes)")
                self.onRoutesReceived(routes)
            } else {
                self.onRoutesError(error!)
            }
        }
        
        let drivingRouter = YMKDirections.sharedInstance().createDrivingRouter()
        drivingSession = drivingRouter.requestRoutes(
            with: requestPoints,
            drivingOptions: YMKDrivingDrivingOptions(),
            vehicleOptions: YMKDrivingVehicleOptions(),
            routeHandler: responseHandler)
    }
    
    func onRoutesReceived(_ routes: [YMKDrivingRoute]) {
        let mapObjects = mapView?.mapWindow.map.mapObjects
        for route in routes {
            mapObjects?.addPolyline(with: route.geometry)
        }
    }
    
    func onRoutesError(_ error: Error) {
        let routingError = (error as NSError).userInfo[YRTUnderlyingErrorKey] as! YRTError
        var errorMessage = "Unknown error"
        if routingError.isKind(of: YRTNetworkError.self) {
            errorMessage = "Network error"
        } else if routingError.isKind(of: YRTRemoteError.self) {
            errorMessage = "Remote server error"
        }
    }
}

class TapListener: NSObject {
    let callback: (MapObjectUserData) -> Void
    
    init(callback: @escaping (MapObjectUserData) -> Void) {
        self.callback = callback
    }
    
}

extension TapListener: YMKMapObjectTapListener {
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        if mapObject is YMKPlacemarkMapObject {
            print("object detected")
            callback(mapObject.userData as! MapObjectUserData)
        }
        return true
    }
}

class LocationObjectListener: NSObject {
    @Binding var mapView: YMKMapView
    
    init(mapView: Binding<YMKMapView>) {
        self._mapView = mapView
    }
}

extension LocationObjectListener: YMKUserLocationObjectListener {
    func onObjectAdded(with view: YMKUserLocationView) {
        print("onObjectAdded")
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {
        print("onObjectAdded")
    }
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {
        print("onObjectAdded")
    }
    
    
}
