//
//  LocationManager.swift
//  YandexMapDemoApp
//
//  Created by Anton on 22.09.2021.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    let manager = CLLocationManager()

    @Published var currentLocation: CLLocationCoordinate2D?
    @Published var location: CLLocationCoordinate2D?
    
    override init() {
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.requestLocation()
    }
    
//    func requestLocation() {
//        manager.requestLocation()
//    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        switch manager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            currentLocation = locations.first?.coordinate
        default:
            print("для определения местоположения нужно разрешить отслеживать ваше геоположение")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        if lhs.longitude == rhs.longitude && lhs.latitude == rhs.latitude {
            return true
        }
        return false
    }
    
    
}
