//
//  Constants.swift
//  YandexMapDemoApp
//
//  Created by Anton on 22.09.2021.
//

import Foundation
import CoreLocation

struct Constants {
    static let cityMoscow = CLLocationCoordinate2D(latitude: 55.755819, longitude: 37.617644)
    static let cityLondon = CLLocationCoordinate2D(latitude: 51.507351, longitude: -0.127696)
    static let cityWashington = CLLocationCoordinate2D(latitude: 47.035519, longitude: -122.897050)
    static let procInMoscow = CLLocationCoordinate2D(latitude: 55.732931, longitude: 37.669088)
    static let procInIvanovo = CLLocationCoordinate2D(latitude: 57.014113, longitude: 40.974210)
    static let procInPeterburg = CLLocationCoordinate2D(latitude: 59.924606, longitude: 30.305206)
}
