//
//  YandexMapDemoAppApp.swift
//  YandexMapDemoApp
//
//  Created by Anton on 22.09.2021.
//

import SwiftUI

@main
struct YandexMapDemoAppApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
